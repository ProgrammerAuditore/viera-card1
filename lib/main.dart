import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      home: Scaffold(
        backgroundColor: Colors.blueGrey,
        appBar: AppBar(
          title: Text('Material App Bar'),
        ),
        body: const MyCard(),
      ),
    );
  }
}

class MyCard extends StatelessWidget {
  const MyCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            ListTile(
                leading: Icon(Icons.photo_album),
                title: Text('Amigos de facebook'),
                subtitle: Text('Boris Parra'),
              ),
              Row(
                children: [
                  TextButton(onPressed: (){}, child: Text('+1 Amigo')),
                  TextButton(onPressed: (){}, child: Text('Firmar muro'))
                ],
              )
          ],
        ),
      ),
    );
  }
}